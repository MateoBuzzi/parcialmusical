﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour
{

    public float rapidezDesplazamiento = 9.0f;
    public Camera camaraEnPrimeraPersona;
    public static bool fin;
    public GameObject finpanel;

    public Text text1;
    public Text text2;
    public Text text3;
    public Text text4;
    public Text text5;
    public Text text6;
    bool heladera = false;
    bool pollo = false;
    public GameObject plato;
    public GameObject comida1;
    public GameObject plato1;
    bool cocinado = false;
    bool servido = false;
    public static int numberC2;
    public Text textC2;





    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        numberC2 = 0;
        fin = false;
        Time.timeScale = 1;
    }
        private void OnTriggerEnter(Collider other)
        {
        if(other.gameObject.CompareTag("Enemigo") == true)
        {
            fin = true;
        }
            if (other.gameObject.CompareTag("Heladera") == true)
            {
                Destroy(text1);
            heladera = true;
             text2.gameObject.SetActive(true);

            }
            if(other.gameObject.CompareTag("Micro") == true && heladera == true)
        {
            Destroy(text2);
            pollo = true;
            text3.gameObject.SetActive(true);
        }
            if(other.gameObject.CompareTag("Mesa")== true && pollo == true)
        {
            Destroy(text3);
            plato.gameObject.SetActive(true);
            text4.gameObject.SetActive(true);
            cocinado = true;
            heladera = false;
        }
            if(other.gameObject.CompareTag("Micro") == true && cocinado == true)
        {
            Destroy(text4);
            text5.gameObject.SetActive(true);
            pollo = false;
            servido = true;
        }
        if (other.gameObject.CompareTag("Mesa") == true && servido == true)
        {
            Destroy(text5);
            comida1.gameObject.SetActive(true);
            text6.gameObject.SetActive(true);
            Destroy(plato1);
        }
        if(other.gameObject.CompareTag("Armario") == true && servido == true)
        {
            SceneManager.LoadScene("Save2");
        }

        }
     
    







    void Update()
    {
        textC2.text = "Recuerdos encontrados: " + numberC2;
 
        if (numberC2 == 3)
        {
            SceneManager.LoadScene("Save4");
        }

        if(fin)
        {
            Time.timeScale = 0;
            finpanel.SetActive(true);
        }

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;


        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

    
        
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }



    }
}

