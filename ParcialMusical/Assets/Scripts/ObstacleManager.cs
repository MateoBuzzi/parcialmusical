﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    public GameObject[] obstaclePrefabs;
    public float zSpawn = 0;
    public float obstacleLenght = 40;
    public int numberObstacle = 5;
    public Transform playerTransform;
    private List<GameObject> activeObstacles = new List<GameObject>();
    
    void Start()
    {
        for (int i = 0; i < numberObstacle; i++)
        {
            if (i == 0)
            {
                spawnObstacle(3);

            }
            else
            {
                spawnObstacle(Random.Range(0, obstaclePrefabs.Length));
            }
        }
  
    }

    // Update is called once per frame
    void Update()
    {
       if(playerTransform.position.y<zSpawn - (numberObstacle * obstacleLenght))
        {
            spawnObstacle(Random.Range(0, obstaclePrefabs.Length));
            
        }
    }
    public void spawnObstacle(int obstacleIndex)
    {
        GameObject go = Instantiate(obstaclePrefabs[obstacleIndex], transform.up*-1 * zSpawn, transform.rotation);
        activeObstacles.Add(go);
        zSpawn += obstacleLenght;
    }
    private void DeleteObstacle()
    {
        Destroy(activeObstacles[0]);
        activeObstacles.RemoveAt(0);
    }
}
