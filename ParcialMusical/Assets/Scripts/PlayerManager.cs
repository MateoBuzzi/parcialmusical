﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PlayerManager : MonoBehaviour
{
    public static bool gameOver;
    public GameObject gameoverPanel;
    public static int numberC;
    public Text textC;
    void Start()
    {
        gameOver = false;
        Time.timeScale = 1;
        numberC = 0;
    }

    // Update is called once per frame
    void Update()
    {
        textC.text = "Coleccionable: " + numberC;
        if(gameOver)
        {
            Time.timeScale = 0;
            gameoverPanel.SetActive(true);
        }
        if(numberC == 25)
        {
            SceneManager.LoadScene("Save3");
        }
    }
}
